package dawcontrol

import (
	"fmt"
	"io"
	"os"

	"gitlab.com/gomonome/monome"
	"gitlab.com/goosc/osc"
)

var _ triggerer = &connection{}
var _ osc.Handler = &connection{}
var _ monome.Connection = &connection{}

type triggerer interface {
	io.Writer // writes to DAW
	monome.Device
}

type handler interface {
	Run()
	setActor(Actor)
	SetTriggerer(triggerer)
	EachMessage(path osc.Path, values ...interface{})
	EachKeyPress(x, y uint8)
	EachKeyRelease(x, y uint8)
}

func (m *connection) findConn(connections []monome.Connection) (monome.Connection, layoutHandler) {
	for _, conn := range connections {
		mp := deviceMapper(conn)

		if mp != nil {
			return conn, mp
		}
	}
	return nil, nil
}

type Connection interface {
	monome.Device
	Matches(path osc.Path) bool
	Connect(dawConnection OSCConnection, errHandler func(error)) error
	Close() error
	Handle(path osc.Path, values ...interface{})
}

type OSCConnection interface {
	io.WriteCloser
	osc.Listener
	Connect() (err error)
}

func deviceMapper(conn monome.Connection) layoutHandler {
	fmt.Printf("connecting to %s\n", conn)
	switch monome.NumButtons(conn) {
	case 64:
		return &m64Layout{}
	case 128:
		return &m128Layout{}
		// case 192:
	default:
		return nil
	}
}

func New(name string, act Actor) Connection {
	mw := &connection{}
	mw.name = name
	mw.actor = act
	return mw
}

type connection struct {
	dawConnection OSCConnection
	monome.Connection
	layout      layoutHandler
	connections []monome.Connection
	name        string
	actor       Actor
}

func (m *connection) String() string {
	return fmt.Sprintf("%s%d", m.name, monome.NumButtons(m))
}

func (m *connection) Switch(x, y uint8, on bool) error {
	err := m.Connection.Switch(x, y, on)
	if err != nil {
		fmt.Fprintf(os.Stderr, "can't write to monome %s: %v", m.Connection, err.Error())
	}
	return err
}

func (m *connection) Set(x, y, brightness uint8) error {
	err := m.Connection.Set(x, y, brightness)
	if err != nil {
		panic("can't write to monome: " + err.Error())
	}
	return err
}

func (m *connection) Write(b []byte) (int, error) {
	return m.dawConnection.Write(b)
}

func (d *connection) Connect(dawConnection OSCConnection, errHandler func(error)) error {
	d.dawConnection = dawConnection
	err := d.dawConnection.Connect()

	if err != nil {
		return err
	}

	connections, err := monome.Connections()

	if err != nil {
		return err
	}

	if len(connections) < 1 {
		return fmt.Errorf("no monome device found")
	}

	conn, m := d.findConn(connections)

	if conn == nil {
		for _, d := range connections {
			d.Close()
		}
		return fmt.Errorf("no monome device found")
	}

	d.connections = connections
	d.Connection = conn
	d.layout = m
	m.SetTriggerer(d)
	d.actor.SetLayout(d.layout)
	d.layout.setActor(d.actor)
	monome.Greeter(d)
	conn.SetHandler(monome.HandlerFunc(d.eachMonomeEvent))
	d.Connection.StartListening(errHandler)
	d.layout.Run()
	return nil
}

func (m *connection) Close() error {
	for _, dev := range m.connections {
		dev.Close()
	}
	m.dawConnection.StopListening()
	m.dawConnection.Close()
	return nil
}

func (m *connection) Matches(path osc.Path) bool {
	return m.actor.Matches(path)
}

// Handle reaper events
func (m *connection) Handle(path osc.Path, values ...interface{}) {
	m.layout.EachMessage(path, values...)
}

// highlight the pressed buttons
func (m *connection) eachMonomeEvent(d monome.Connection, x, y uint8, down bool) {
	if down {
		m.layout.EachKeyPress(x, y)
		return
	}
	m.layout.EachKeyRelease(x, y)
}
