package dawcontrol

/*
TODO:

we want the following behavior:

DELETE/FILL

when the delete button is pressed and released, nothing
happens to any clip be it selected or not.

while the delete button is pressed, the filled clips
(that are otherwise hidden), are shown.

if a clip button is pressed, while the delete button is pressed,
the clips switch their state, i.e. filled clips become deleted
and not filled clips became marked as filled (good way to sync
to an existing playtime session)

the selection state is not effected by this

TRIGGER

if the button is pressed, the filled clips are shown and a clip
might be selected/ the selected clip might be changed by selecting
another one while the button is pressed.

when the button is released, the clip that is in this moment selected
is triggered.

this has the effect to be able to quickly change and trigger a clip
when using the trigger button as a "shift" key and also be able to
"prepare" a selection by first selecting the clip and the fast triggering
always via the same button/position, so that you don't have to think about
the position when concentrating on the trigger moment. in fact, the finger could
just rest on the button and be released in the very last moment and by this triggering
more reliably. also it is easy to stop the last recorded clip by pressing the
triggering button again (not having to lookup the clip).

to just lookup where we have clips could be done by pressing the delete button.
to lookup just before triggering is done by pressing the trigger button.


*/

import (
	"io"

	"gitlab.com/goosc/osc"
)

type Actor interface {
	ActionPressed(btn ActionButton)
	StopAllClips()
	TrackStop(track uint8)
	ScenePressed(scene uint8) bool
	ClipPressed(track, clip uint8)
	ActionReleased(btn ActionButton)
	EachMessage(path osc.Path, vals ...interface{})
	InShowFilledMode() bool
	InFillOrDeleteMode() bool
	IsPlaying() bool
	SelectedClip() (track, clip uint8)
	HasSelectedClip() bool
	SetLayout(Layout)
	Matches(path osc.Path) bool
}

type layoutHandler interface {
	handler
	Layout
}

type Layout interface {
	io.Writer
	PrintActiveClips()
	ActiveTrackClip(track uint8) (clip uint8)
	IsFilledClip(track uint8, clip uint8) bool
	FillClip(track uint8, clip uint8)
	UnfillClip(track uint8, clip uint8)
	SetActiveTrackClip(track uint8, clip uint8)
	BlankAllClips()
	BlankActiveTrackClips()
	SwitchClip(track, clip uint8, on bool)
	SwitchTrack(track uint8, on bool)
	SwitchActionButton(btn ActionButton, on bool)
	PrintFilled()
	NumTracks() uint8
	NumClipsPerTrack() uint8
}

type ActionButton uint8

const (
	TRIGGER_BUTTON    ActionButton = 0
	PLAY_BUTTON       ActionButton = 1
	UNDO_BUTTON       ActionButton = 2
	DELETE_BUTTON     ActionButton = 3
	AUTOMATION_BUTTON ActionButton = 4
	OVERDUB_BUTTON    ActionButton = 5
	METRONOME_BUTTON  ActionButton = 6
	TEMPO_TAP_BUTTON  ActionButton = 7
)
