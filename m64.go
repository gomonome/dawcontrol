package dawcontrol

import (
	"sync"
	"time"

	"gitlab.com/goosc/osc"
)

type m64Layout struct {
	cache           [8][8]bool
	last            [8][8]bool
	filledClips     [7][6]bool
	activeTrackClip [7]uint8
	trackMuted      [7]bool
	trackClip       [7]uint8 // val % 7 = clip number; val / 7 : 0 not playing, not recording, 1 : playing, 2: recording
	actionButtons   map[uint8]ActionButton
	actor           Actor
	triggerer
	sync.RWMutex
}

func (m *m64Layout) setActor(act Actor) {
	m.actor = act
}

func (m *m64Layout) Set(x, y, brightness uint8) {
	m.Switch(x, y, brightness > 0)
}

func (m *m64Layout) Switch(x, y uint8, on bool) {
	m.Lock()
	m.cache[int(x)][int(y)] = on
	m.Unlock()
}

// Flush flushes the cache to the underlying device
func (m *m64Layout) Flush() {
	m.Lock()
	for x, mp := range m.cache {
		for y, on := range mp {
			if on != m.last[x][y] {
				m.triggerer.Switch(uint8(x), uint8(y), on)
			}
		}
	}
	m.last = m.cache
	m.Unlock()
}

func (p *m64Layout) blinkSelected() {
	var hasSelectedClip bool
	var selectedClipTrack, selectedClipClip uint8
	var fillOrDeleteMode bool
	var showFilledMode bool

	for {
		fillOrDeleteMode = p.actor.InFillOrDeleteMode()
		showFilledMode = p.actor.InShowFilledMode()

		if showFilledMode {
			p.PrintFilled()
		}

		p.Flush()

		if !fillOrDeleteMode {
			hasSelectedClip = p.actor.HasSelectedClip()
			if hasSelectedClip {
				selectedClipTrack, selectedClipClip = p.actor.SelectedClip()
				p.SwitchClip(selectedClipTrack, selectedClipClip, true)
				p.Flush()
				time.Sleep(time.Millisecond * 200)
				p.SwitchClip(selectedClipTrack, selectedClipClip, false)
				p.Flush()
				//				time.Sleep(time.Millisecond * 100)
			}
		}

		time.Sleep(time.Millisecond * 200)
	}
}

func (p *m64Layout) blinkRunning() {
	var isPlaying bool
	var showFilledMode bool

	for {
		isPlaying = p.actor.IsPlaying()
		showFilledMode = p.actor.InShowFilledMode()

		if !showFilledMode {
			p.PrintActiveClips()
		}

		if isPlaying {
			p.SwitchActionButton(PLAY_BUTTON, true)
			time.Sleep(time.Millisecond * 300)
			p.SwitchActionButton(PLAY_BUTTON, false)
		} else {
			p.SwitchActionButton(PLAY_BUTTON, false)
		}

		p.Flush()
		time.Sleep(time.Millisecond * 300)
	}
}

func (m *m64Layout) EachMessage(path osc.Path, vals ...interface{}) {
	m.actor.EachMessage(path, vals...)
	m.Flush()
}

func (m *m64Layout) SwitchActionButton(btn ActionButton, on bool) {
	var y uint8
	var found bool

	for _y, b := range m.actionButtons {
		if btn == b {
			found = true
			y = _y
		}
	}

	if found {
		m.Switch(7, y, on)
	}
}

func (m *m64Layout) BlankActiveTrackClips() {

	for y, cl := range m.activeTrackClip {
		if cl > 0 {
			m.Switch(cl-1, uint8(y), false)
		}
	}
	m.activeTrackClip = [7]uint8{}

}

// 0 = no active clip
func (m *m64Layout) ActiveTrackClip(track uint8) uint8 {
	m.RLock()
	var cl uint8 = m.activeTrackClip[int(track)]
	m.RUnlock()
	return cl
}

func (m *m64Layout) SetActiveTrackClip(track uint8, clip uint8) {
	m.Lock()
	m.activeTrackClip[int(track)] = clip
	m.Unlock()
	//m.PrintActiveClips()
	//m.Flush()
}

func (m *m64Layout) IsFilledClip(track uint8, clip uint8) bool {
	m.RLock()
	is := m.filledClips[int(track)][int(clip)]
	m.RUnlock()
	return is
}

func (m *m64Layout) FillClip(track uint8, clip uint8) {
	m.Lock()
	m.filledClips[int(track)][int(clip)] = true
	m.Unlock()
}

func (m *m64Layout) UnfillClip(track uint8, clip uint8) {
	m.Lock()
	m.filledClips[int(track)][int(clip)] = false
	m.Unlock()
}

func (m *m64Layout) SwitchClip(track, clip uint8, on bool) {
	m.Switch(clip, track, on)
}

func (m *m64Layout) PrintActiveClips() {
	m.RLock()
	activeTrackClip := m.activeTrackClip
	m.RUnlock()

	for y := uint8(0); y < m.NumTracks(); y++ {
		for x := uint8(0); x < m.NumClipsPerTrack(); x++ {
			m.Switch(x, y, activeTrackClip[y] == x+1)
		}
	}
	/*
		for y, cl := range activeTrackClip {
			if cl > 0 {
				m.Switch(cl-1, uint8(y), cl > 0)
			}
		}
	*/

	/*
		time.Sleep(time.Millisecond * 200)

		for y, cl := range activeTrackClip {
			if cl > 0 {
				m.Switch(cl-1, uint8(y), false)
			}
		}
	*/
}

func (m *m64Layout) PrintFilled() {
	m.BlankAllClips()
	for y, cl := range m.filledClips {
		for x, filled := range cl {
			m.Switch(uint8(x), uint8(y), filled)
		}
	}
}

func (m *m64Layout) Run() {
	m.actionButtons = map[uint8]ActionButton{
		0: TRIGGER_BUTTON,
		1: PLAY_BUTTON,
		2: UNDO_BUTTON,
		3: DELETE_BUTTON,
		4: AUTOMATION_BUTTON,
		5: OVERDUB_BUTTON,
		6: METRONOME_BUTTON,
		7: TEMPO_TAP_BUTTON,
	}
	//	m.blinker = &blinker64{m, act}
	go m.blinkRunning()
	go m.blinkSelected()
	//	go m.filledChecked()
}

var _ handler = &m64Layout{}

func (m *m64Layout) SwitchTrack(track uint8, on bool) {
	if track < 7 {
		m.Switch(6, track, on)
	}
	m.Flush()
}

func (m *m64Layout) EachKeyPress(x, y uint8) {
	defer m.Flush()
	switch x {
	case 7:
		m.actor.ActionPressed(m.actionButtons[y])
		return
	case 6:
		if y == 7 {
			m.actor.StopAllClips()
			return
		}
		m.actor.TrackStop(y)
		m.Switch(x, y, true)
		return
	}

	switch y {
	case 7:
		switchOn := m.actor.ScenePressed(x)
		m.Switch(x, y, switchOn)
		return
	default:
		m.actor.ClipPressed(y, x)
	}
}

func (m *m64Layout) BlankAllClips() {
	for x := uint8(0); x < 6; x++ {
		for y := uint8(0); y < 7; y++ {
			m.Switch(x, y, false)
		}
	}
}

func (m *m64Layout) NumTracks() uint8 {
	return 7
}

func (m *m64Layout) NumClipsPerTrack() uint8 {
	return 6
}

func (m *m64Layout) EachKeyRelease(x, y uint8) {
	defer m.Flush()
	if x == 7 {
		m.actor.ActionReleased(m.actionButtons[y])
		return
	}

	if x == 6 {
		m.Switch(x, y, false)
		return
	}

	if y == 7 {
		m.Switch(x, y, false)
	}

	if !m.actor.InShowFilledMode() {
		m.Switch(x, y, false)
	}
}

func (m *m64Layout) SetTriggerer(tr triggerer) {
	m.triggerer = tr
}
